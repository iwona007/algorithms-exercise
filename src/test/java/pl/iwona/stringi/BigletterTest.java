package pl.iwona.stringi;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BigletterTest {

    @Test
    @DisplayName("Ever new word should began from upperCase")
    void splitStringTest() {
        Bigletter bigletter = new Bigletter();
        String word = "slowo slowo";
        assertEquals("Slowo Slowo", bigletter.splitString(word));
    }

    @Test
    @DisplayName("Should return null if it is an emplty word")
    void shouldReturnNullIfItIsAnEmptyWord() {
        Bigletter bigletter = new Bigletter();
        String word = "";
        assertEquals(null, bigletter.splitString(word));
    }

    @Test
    @DisplayName("Should start from upperCase after each space")
    void ShouldStartFromUpperCaseAfterEachSpace() {
        Bigletter bigletter = new Bigletter();
        String word = "wyraz sklada sie z szesciu slow";
        assertEquals("Wyraz Sklada Sie Z Szesciu Slow", bigletter.startFromUpperCase(word));
    }
}
