package pl.iwona.stringi;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InicjalyTest {

    @Test
    @DisplayName("Should show only upperCase initials test")
    void inicjalTest() {
        String name = "ela nowak";
        String expected = "E.N";
        assertEquals(expected, Inicjaly.inicjal(name));
        assertEquals(expected, Inicjaly.inicjal2(name));
        assertEquals(expected, Inicjaly.initial(name));
    }
}

