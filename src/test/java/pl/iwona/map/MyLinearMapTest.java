/**
 * 
 */
package pl.iwona.map;

import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author downey
 *
 */
public class MyLinearMapTest {

   protected Map<String, Integer> map;

   /**
    * @throws Exception
    */
   @Before
   public void setUp() throws Exception {
      map = new MyLinearMap<String, Integer>();
      map.put("Jeden", 1);
      map.put("Dwa", 2);
      map.put("Trzy", 3);
      map.put(null, 0);
   }

   /**
    * Metoda testująca {@link MyLinearMap#clear()}.
    */
   @Test
   public void testClear() {
      map.clear();
      assertThat(map.size(), is(0));
   }

   /**
    * Metoda testująca {@link MyLinearMap#containsKey(Object)}.
    */
   @Test
   public void testContainsKey() {
      assertThat(map.containsKey("Trzy"), is(true));
      assertThat(map.containsKey(null), is(true));
      assertThat(map.containsKey("Cztery"), is(false));
   }

   /**
    * Metoda testująca {@link MyLinearMap#containsValue(Object)}.
    */
   @Test
   public void testContainsValue() {
      assertThat(map.containsValue(3), is(true));
      assertThat(map.containsValue(0), is(true));
      assertThat(map.containsValue(4), is(false));
   }

   /**
    * Metoda testująca {@link MyLinearMap#get(Object)}.
    */
   @Test
   public void testGet() {
      assertThat(map.get("Trzy"), is(3));
      assertThat(map.get(null), is(0));
      assertThat(map.get("Cztery"), nullValue());
   }

   /**
    * Test method for {@link MyLinearMap#isEmpty()}.
    */
   @Test
   public void testIsEmpty() {
      assertThat(map.isEmpty(), is(false));
      map.clear();
      assertThat(map.isEmpty(), is(true));
   }

   /**
    * Test method for {@link MyLinearMap#keySet()}.
    */
   @Test
   public void testKeySet() {
      Set<String> keySet = map.keySet();
      assertThat(keySet.size(), is(4));
      assertThat(keySet.contains("Trzy"), is(true));
      assertThat(keySet.contains(null), is(true));
      assertThat(keySet.contains("Cztery"), is(false));
   }

   /**
    * Test method for {@link MyLinearMap#put(Object, Object)}.
    */
   @Test
   public void testPut() {
      map.put("Jeden", 11);
      assertThat(map.size(), is(4));
      assertThat(map.get("Jeden"), is(11));
      
      map.put("Pięć", 5);
      assertThat(map.size(), is(5));
      assertThat(map.get("Pięć"), is(5));
   }

   /**
    * Test method for {@link MyLinearMap#putAll(Map)}.
    */
   @Test
   public void testPutAll() {
      Map<String, Integer> m = new HashMap<String, Integer>();
      m.put("Sześć", 6);
      m.put("Siedem", 7);
      m.put("Osiem", 8);
      map.putAll(m);
      assertThat(map.size(), is(7));
   }

   /**
    * Test method for {@link MyLinearMap#remove(Object)}.
    */
   @Test
   public void testRemove() {
      map.remove("Jeden");
      assertThat(map.size(), is(3));
      assertThat(map.get("Jeden"), nullValue());
   }

   /**
    * Test method for {@link MyLinearMap#size()}.
    */
   @Test
   public void testSize() {
      assertThat(map.size(), is(4));
   }

   /**
    * Test method for {@link MyLinearMap#values()}.
    */
   @Test
   public void testValues() {
      Collection<Integer> keySet = map.values();
      assertThat(keySet.size(), is(4));
      assertThat(keySet.contains(3), is(true));
      assertThat(keySet.contains(0), is(true));
      assertThat(keySet.contains(4), is(false));
   }
}
