package pl.iwona.array;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MinTest {

    @Test
    @DisplayName("Should find min number in array test")
    void findMinInArrayTest() {
        Min min = new Min();
        int expected = -2;
        int actual = min.findMinInArray(new int[]{9, 50, -2, 0, 10});
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Should find min value in array with negative value array test")
    void findMinInArrayTest2() {
        Min min = new Min();
        int expected = -50;
        int actual = min.findMinInArray(new int[]{-50, -2, -10, 0, -5, -8});
        int actual2 = min.findMinInArray2(new int[]{-50, -2, -10, 0, -5, -8});
        assertEquals(expected, actual);
        assertEquals(expected, actual2);
    }

    @Test
    @DisplayName("Should find min value in positive value array test ")
    void findMinInArrayTest3() {
        Min min = new Min();
        int expected = 5;
        int actual = min.findMinInArray(new int[]{50, 200, 10, 8, 5, 18});
        assertEquals(expected, actual);
    }
}
