package pl.iwona.integer;

import java.util.Random;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PowerTest {

    @Test
    @DisplayName("Should power each number separately")
    void powerDigitTest() {
        Power power = new Power();
        int actual = power.powerDigit(9119);
        int actual2 = power.powerDigit2(9119);
        int expected = 811181;
        assertEquals(expected, actual);
        assertEquals(expected, actual2);
    }
}
