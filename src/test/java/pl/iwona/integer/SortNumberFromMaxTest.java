package pl.iwona.integer;

import org.junit.jupiter.api.Test;
import pl.iwona.integer.SortNumberFromMax;

import static org.junit.jupiter.api.Assertions.*;

class SortNumberFromMaxTest {

    @Test
    void sortDesc() {
        int num = 0;
        assertEquals(0, SortNumberFromMax.sortDesc(num));
    }

    @Test
    void shouldShowMaxToMinTest() {
        int num = 15;
        assertEquals(51, SortNumberFromMax.sortDesc(num));
    }

    @Test
    void shouldShowMaxToMinTest2() {
        int num = 255478931;
        assertEquals(987554321, SortNumberFromMax.sortDesc(num));
    }
}
