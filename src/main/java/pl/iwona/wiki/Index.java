package pl.iwona.wiki;

import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Hermetyzuje odwzorowanie wyszukiwanego słowa na obiekt klasy TermCounter.
 *
 * @author downey
 *
 */
public class Index {

    private Map<String, Set<TermCounter>> index = new HashMap<String, Set<TermCounter>>();

    /**
     * Dodaje obiekt klasy TermCounter do zbioru związanego z argumentem 'term'.
     *
     * @param term
     * @param tc
     */
    public void add(String term, TermCounter tc) {
        Set<TermCounter> set = get(term);

        // jeśli słowo występuje po raz pierwszy, utwórz nowy zbiór
        if (set == null) {
            set = new HashSet<TermCounter>();
            index.put(term, set);
        }
        // w przeciwnym razie można zmodyfikować istniejący zbiór
        set.add(tc);
    }

    /**
     * Wyszukuje szukane słowo i zwraca zbiór obiektów klasy TermCounter.
     *
     * @param term
     * @return
     */
    public Set<TermCounter> get(String term) {
        return index.get(term);
    }

   /**
    * Wyświetla zawartość indeksu.
    */
   public void printIndex() {
      // przejście przez wyszukiwane słowa
      for (String term: keySet()) {
         System.out.println(term);
         
         // dla każdego słowa wyświetlenie miejsca, w którym się pojawia, i częstości występowania
         Set<TermCounter> tcs = get(term);
         for (TermCounter tc: tcs) {
            Integer count = tc.get(term);
            System.out.println("    " + tc.getLabel() + " " + count);
         }
      }
   }

   /**
    * Zwraca zbiór słów, które zostały zindeksowane.
    * 
    * @return
    */
   public Set<String> keySet() {
      return index.keySet();
   }

   /**
    * Dodaje stronę do indeksu.
    *
    * @param url         URL strony.
    * @param paragraphs  Kolekcja elementów, które powinny być zindeksowane.
    */
   public void indexPage(String url, Elements paragraphs) {
      // utwórz obiekt klasy TermCounter i zlicz wystąpienia wyszukiwanych słów w akapitach
      TermCounter tc = new TermCounter(url);
      tc.processElements(paragraphs);
      
      // dla każdego wyszukiwanego słowa w obiekcie TermCounter dodaj obiekt TermCounter do indeksu
      for (String term: tc.keySet()) {
         add(term, tc);
      }
   }

   /**
    * @param args
    * @throws IOException 
    */
   public static void main(String[] args) throws IOException {
      
      WikiFetcher wf = new WikiFetcher();
      Index indexer = new Index();

      String url = "https://en.wikipedia.org/wiki/Java_(programming_language)";
      Elements paragraphs = wf.fetchWikipedia(url);
      indexer.indexPage(url, paragraphs);
      
      url = "https://en.wikipedia.org/wiki/Programming_language";
      paragraphs = wf.fetchWikipedia(url);
      indexer.indexPage(url, paragraphs);
      
      indexer.printIndex();
   }
}