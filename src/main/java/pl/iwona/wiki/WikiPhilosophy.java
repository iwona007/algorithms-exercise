package pl.iwona.wiki;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WikiPhilosophy {

    final static List<String> visited = new ArrayList<>();
    final static WikiFetcher wf = new WikiFetcher();

    /**
     * Sprawdza hipotezę dotyczącą Wikipedii i filozofii.
     * <p>
     * https://en.wikipedia.org/wiki/Wikipedia:Getting_to_Philosophy
     * <p>
     * 1. Kliknij pierwsze łącze, które nie znajduje się w nawiasie i nie jest wyświetlane pochyloną czcionką.
     * 2. Pomiń łącza zewnętrzne, łącza wskazujące bieżącą stronę oraz czerwone łącza.
     * 3. Zakończ, gdy dojdzie do strony hasła "filozofia", strony bez łączy lub strony, która nie istnieje,
     * albo gdy natrafisz na pętlę.
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String destination = "https://en.wikipedia.org/wiki/Philosophy";
        String source = "https://en.wikipedia.org/wiki/Java_(programming_language)";

        testConjecture(destination, source, 10);
    }

    /**
     * Zaczyna od podanego URL-a i przechodzi na stronę wskazywaną przez pierwsze łącze aż do momentu,
     * gdy uda się dotrzeć do strony docelowej lub przekroczony zostanie limit.
     *
     * @param destination
     * @param source
     * @throws IOException
     */
    public static void testConjecture(String destination, String source, int limit) throws IOException {
        //  UZUPEŁNIJ TEN KOD!
        String url = source;


        for (int i = 0; i < limit; i++) {
            if (visited.contains(url)) {
                System.err.println("Ten link już był. Weszliśmy do petli kończymy");
                return;
            } else {
                visited.add(url);
            }

            Element element = findFirstValidLink(url);
            if(element == null || element.equals("")) {
                System.err.println("Dotarliśmy do strony bez prawidłowych łączy.");
                return;
            }


            System.out.println("**" + element.text() + "**");
            url = element.attr("abs:href");


            if (url.equals(destination)) {
                System.out.println("Znaleziono filozofię. Koniec pracy" + destination);
                break;
            }
        }

    }


    public static Element findFirstValidLink(String url) throws IOException {
        print("Pobieram prawidłowe łącze: ", url);
        Elements paragraphs = wf.fetchWikipedia(url);
        WikiParser wp = new WikiParser(paragraphs);
        return wp.findFirstLink();

    }


    public static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

}