package pl.iwona.integer;

/*
input np. 21445
output np. 54421
 */

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

public class SortNumberFromMax {
    public static void main(String[] args) {
        int number = 123456789;
        System.out.println(sortDesc(number));
        System.out.println(sortDesc2(number));
    }

    public static int sortDesc(final int num) {
     return Integer.parseInt(String.valueOf(num).chars()
             .mapToObj(x -> String.valueOf(Character.getNumericValue(x)))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining()));
    }

    public static int sortDesc2(final int num) {
        String[] number = String.valueOf(num).split("");
        Arrays.sort(number, Collections.reverseOrder());
       return Integer.parseInt(String.join("", number));
    }
}
