package pl.iwona.integer;

import java.util.stream.Collectors;

public class Power {
    public static void main(String[] args) {
        Power power = new Power();
        System.out.println(power.powerDigit(9119));
        System.out.println(power.powerDigit2(9119));
    }

    public int powerDigit(int num) {
        String number = String.valueOf(num);
        String result = "";
        for (char c : number.toCharArray()) {
            int digit = Character.digit(c, 10);

            result += digit * digit;
        }
        return Integer.parseInt(result);
    }

    public int powerDigit2(int num) {
        return Integer.parseInt(String.valueOf(num).chars()
                .mapToObj(x -> Character.getNumericValue(x))
                .map(x -> x * x)
                .map(String::valueOf)
                .collect(Collectors.joining("")));
    }
}
