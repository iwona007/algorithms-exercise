package pl.iwona.stringi;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Bigletter {
    public static void main(String[] args) {
        Bigletter bigletter = new Bigletter();
        String word = "slowo kluczowe tygodnia";
        bigletter.splitString(word);
        bigletter.startFromUpperCase(word);
        bigletter.startFromUpperCase2(word);
    }

    public String splitString(String text) {
        if (text.equals("") || text == null) {
            return null;
        }
//        x =? x->X
        char[] array = text.toCharArray();
        for (int x = 0; x < array.length; x++) {
            if (x == 0 || array[x - 1] == ' ') {
                array[x] = Character.toUpperCase(array[x]);
            }
        }
        System.out.println(array);
        return new String(array);
    }

    public String startFromUpperCase(String text) {
        if (text.equals("") || text == null) {
            return null;
        }
        return Arrays.stream(text.split(" "))
                .map(x -> x.substring(0, 1).toUpperCase() + x.substring(1, x.length()))
                .collect(Collectors.joining(" "));
    }

    public String startFromUpperCase2(String text) {
        if (text.equals("") || text == null) {
            return null;
        }
        String[] splittedString = text.split(" ");
        String[] oneWord = Arrays.stream(splittedString)
                .map(x -> x.substring(0,1).toUpperCase() + x.substring(1))
                .toArray(size -> new String[size]);
        return String.join("", oneWord);
    }


}
