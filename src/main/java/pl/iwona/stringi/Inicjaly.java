package pl.iwona.stringi;

import java.util.Arrays;
import java.util.stream.Collectors;
/*
Inicjaly mają byc z dużych liter połączone "."
 */
public class Inicjaly {
    public static void main(String[] args) {
        String name = "magda kowalska";
        System.out.println(inicjal(name));
        System.out.println(inicjal2(name));
        System.out.println(initial(name));
    }

    public static String inicjal(String name) {
//        String[] newName = name.split(" ");
        return Arrays.stream(name.split(" "))
                .map(x -> x.substring(0, 1).toUpperCase())
                .collect(Collectors.joining("."));
    }

    public static String inicjal2(String name) {
       name = name.toUpperCase();
       name = name.charAt(0) + "." + (name.charAt(name.indexOf(" ") +1));
       return name;
    }

    public static String initial(String name) {
        String[] split = name.split(" ");
      return  split[0].substring(0,1).toUpperCase().concat("." + split[1].substring(0,1).toUpperCase());
    }
}
