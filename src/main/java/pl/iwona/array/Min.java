package pl.iwona.array;

import java.util.Arrays;

public class Min {
    public static void main(String[] args) {
        Min min = new Min();
        int[] num = new int[]{200, 8, 10, 0, -16};
        System.out.println(min.findMinInArray(num));
        System.out.println(min.findMinInArray2(num));
    }

    public int findMinInArray(int[] numbers) {
        int min = numbers[0];
        for (int i = 0; i < numbers.length; i++) {
            if (min > numbers[i]) {
                min = numbers[i];
            }
        }
        return min;
    }

    public int findMinInArray2(int[] numbers) {
        Arrays.sort(numbers);
        return numbers[0];
    }
}
